public class Worker {
    private String name;
    private FileReader fileReader;

    public Worker() {
        name = "worker";
    }

    public Worker(String name) {
        this.name = name;
    }

    public boolean isPalindrome(String str) {
        boolean isPalindrome = true;

        for (int i = 0, j = str.length() - 1; i != j; i++, j--) {
            if (str.charAt(i) != str.charAt(j)) {
                isPalindrome = false;
                break;
            }
        }

        return isPalindrome;
    }

    public long getFactorial(int num) {
        return num > 1 ? num * getFactorial(num - 1) : 1;
    }

    public void haveFunWithIntegers() {
        int[] integers = fileReader.getNumbersFromFile();

        long factorialSum = 0;
        for (int num : integers) {
            factorialSum += getFactorial(num);
        }

        if (factorialSum < 500) {
            integers = fileReader.getNumbersFromFileUsingInterface();
        }
    }

    public int getNameLength() {
        return this.name.length();
    }

    public String getName() {
        return this.name;
    }

    public void setFileReader(FileReader fileReader) {
        this.fileReader = fileReader;
    }

}
