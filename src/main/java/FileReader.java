
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileReader {
    private FileWorker fileWorker;

    public FileReader() {

    }

    public FileReader(FileWorker fileWorker) {
        this.fileWorker = fileWorker;
    }

    public int[] getNumbersFromFile() {
        Path filePath = Paths.get("C:\\Users\\sashk\\Desktop\\string\\task09_JUNIT\\integers.txt");
        List<Integer> arrayList = new ArrayList<>();

        try {
        Scanner scanner = new Scanner(filePath);
        while (scanner.hasNext()) {
            if (scanner.hasNextInt()) {
                arrayList.add(scanner.nextInt());
            } else {
                scanner.next();
            }
        }

        } catch (java.io.IOException ex) {
            System.out.println("something went wrong");
        }

        return arrayList
                .stream().mapToInt(x -> x).toArray();
    }

    public int[] getNumbersFromFileUsingInterface() {
        return fileWorker.getNumbersFromFile();
    }
}
