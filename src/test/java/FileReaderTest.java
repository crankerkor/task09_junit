import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class FileReaderTest {
    @Test
    public void testGetNumbersFromFile() {
        FileReader fileReader = new FileReader();
        int[] numbersFromFile = fileReader.getNumbersFromFile();
        int[] expectedNumbers = new int[]{17, 9, 1, 8, 91};

        assertArrayEquals(expectedNumbers, numbersFromFile);
    }

}
