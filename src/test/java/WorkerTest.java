
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WorkerTest {

    private static int testCounter = 0;

    @InjectMocks
    FileReader reader;

    @Mock
    FileWorker fileWorker;

    @Mock
    FileReader fr;

    @BeforeAll
    public static void beforeAll() {
        System.out.println("Before all: " + testCounter);
    }

    @AfterAll
    public static void afterAll() {
        System.out.println("After all: " + testCounter);
    }

    @Test
    public void testIsPalindrome() {
        testCounter++;
        Worker worker = new Worker();

        boolean isPalindrome = worker.isPalindrome("racecar");
        assertTrue(isPalindrome, "Racecar is palindrome");

        isPalindrome = worker.isPalindrome("palindrome");
        assertFalse(isPalindrome, "Palindrome is not palindrome");
    }

    @Test
    public void testGetNameLength() {
        testCounter++;

        Worker usualWorker = new Worker();
        int length = usualWorker.getNameLength();

        assertEquals(6, length);

        Worker namedWorker = new Worker("jack");
        length = namedWorker.getNameLength();

        assertNotEquals(10, length);
    }

    @Test
    public void testGetFactorial() {
        testCounter++;

        when(fileWorker.getNumbersFromFile()).thenReturn(new int[] {15, 5});

        int[] arrayFromMock = reader.getNumbersFromFileUsingInterface();

        Worker worker = new Worker();
        long factorial = worker.getFactorial(arrayFromMock[1]);

        assertEquals(120, factorial);
    }

    @Test
    public void testHaveFunWithIntegers() {
        testCounter++;

        when(fr.getNumbersFromFile()).thenReturn(new int[] {5, 7, 10, 15});

        Worker integerWorker = new Worker();
        integerWorker.setFileReader(fr);

        integerWorker.haveFunWithIntegers();

        verify(fr).getNumbersFromFile();

        verify(fr, times(0)).getNumbersFromFileUsingInterface();
    }

}
